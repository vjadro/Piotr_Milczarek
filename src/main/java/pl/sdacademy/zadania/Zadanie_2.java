package pl.sdacademy.zadania;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadanie_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] numbers = new int[3];

        System.out.println("Podaj 3 liczby:");
        try {
            numbers[0] = sc.nextInt();
            numbers[1] = sc.nextInt();
            numbers[2] = sc.nextInt();
        } catch(InputMismatchException e) {
            System.out.println("Dupa, nara!");
            return;
        }

        int sum = 0;

        for (int number : numbers) {
            if (isEven(number)) sum += number;
        }

        System.out.println("Suma liczb parzystych: " + sum);
    }

    public static boolean isEven(int number) {
        return number%2 == 0 ? true : false;
    }
}
