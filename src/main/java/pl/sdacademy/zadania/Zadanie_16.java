package pl.sdacademy.zadania;

import java.util.Scanner;

public class Zadanie_16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj pesel: ");
        String pesel = sc.nextLine();

        if (pesel.length() != 11) {
            System.out.println("Nieprawidłowy pesel.");
            return;
        }

        try {
            long p = Long.parseLong(pesel);
        } catch (NumberFormatException e) {
            System.out.println("Nieprawidlowe znaki.");
            return;
        }

        int[] w = {9,7,3,1,9,7,3,1,9,7};

        int sumNumber = 0;

        for (int i = 0; i < w.length; i++) {
            sumNumber += (pesel.charAt(i)-48) * w[i];
        }

        if (sumNumber % 10 == pesel.charAt(10)-48)
            System.out.println("Prawidlowy pesel.");
        else
            System.out.println("Nieprawidlowy pesel.");
    }
}
