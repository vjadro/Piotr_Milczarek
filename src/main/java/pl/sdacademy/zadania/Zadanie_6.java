package pl.sdacademy.zadania;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Zadanie_6 {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();

        int number;

        System.out.println("Podawaj liczby calkowite, lub 0 aby zakończyć.");

        do {
            number = getValidInt();
            if (number != 0) numbers.add(number);
        } while(number != 0);

        int numberOfOdds = 0;
        int numberOfEvens = 0;

        for (int num : numbers) {
            if (num % 2 == 0)
                numberOfEvens++;
            else
                numberOfOdds++;
        }

        System.out.println("Liczba wszystkich elementów: " + numbers.size());
        System.out.println("Liczba parzystych: " + numberOfEvens);
        System.out.println("Liczba nieparzystych: " + numberOfOdds);
    }

    public static int getValidInt() {
        Scanner sc = new Scanner(System.in);

        int number = 0;

        while(true) {
            System.out.println("Podaj liczbe: ");
            try {
                number = sc.nextInt();
                break;
            } catch(InputMismatchException e) {
                sc.nextLine();
                System.out.println("Bledne dane!");
            }
        }

        return number;
    }
}
