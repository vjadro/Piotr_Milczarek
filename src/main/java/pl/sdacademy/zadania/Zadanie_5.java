package pl.sdacademy.zadania;

public class Zadanie_5 {
    public static void main(String[] args) {
        int[] numbers = {10,31,12,13,7,15,16,17,18,19,20};

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        int minIndex = 0, maxIndex = 0;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] < min) {
                min = numbers[i];
                minIndex = i;
            }
            if (numbers[i] > max) {
                max = numbers[i];
                maxIndex = i;
            }
        }

        System.out.println("Min value: " + min + " (" + minIndex + ")");
        System.out.println("Max value: " + max + " (" + maxIndex + ")");
    }
}
