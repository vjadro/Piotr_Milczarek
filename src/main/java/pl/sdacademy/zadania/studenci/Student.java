package pl.sdacademy.zadania.studenci;

import java.util.Map;

public class Student {
    private String fName;
    private String lName;
    private long indexNumber;
    private int yearOfBirth;
    private Map<Subject,Integer> grades;

    public Student(String fName, String lName, long indexNumber, int yearOfBirth, Map<Subject, Integer> grades) {
        this.fName = fName;
        this.lName = lName;
        this.indexNumber = indexNumber;
        this.yearOfBirth = yearOfBirth;
        this.grades = grades;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public long getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(long indexNumber) {
        this.indexNumber = indexNumber;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public Map<Subject, Integer> getGrades() {
        return grades;
    }

    public void setGrades(Map<Subject, Integer> grades) {
        this.grades = grades;
    }

    @Override
    public String toString() {
        return "Student{" +
                "fName='" + fName + '\'' +
                ", lName='" + lName + '\'' +
                ", indexNumber=" + indexNumber +
                ", yearOfBirth=" + yearOfBirth +
                ", grades=" + grades +
                '}';
    }
}
