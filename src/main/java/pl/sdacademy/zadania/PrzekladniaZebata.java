package pl.sdacademy.zadania;

import java.util.ArrayList;
import java.util.List;

public class PrzekladniaZebata {
    public static void main(String[] args) {
        List<Integer> gears = new ArrayList<>();
        gears.add(100); // 60
        gears.add(300); // 180
        gears.add(200); // 120
        gears.add(50);  // 30

        float ratio = 1;

        for (int i = 0; i < gears.size() - 1; i++) {
            ratio *= rotationRatio(gears.get(i), gears.get(i+1));
        }

        System.out.println(ratio*60);
    }

    public static float rotationRatio(int a, int b) {
        return Math.abs((float)b/a);
    }
}
