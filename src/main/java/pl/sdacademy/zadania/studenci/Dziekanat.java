package pl.sdacademy.zadania.studenci;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Dziekanat {

    private List<Student> students;

    public Dziekanat() {
        students = new ArrayList<>();
    }

    public boolean addStudent(Student student) {
        for(Student s : students) {
            if (s.getIndexNumber() == student.getIndexNumber())
                return false;
        }

        students.add(student);
        return true;
    }

    public Student getStudent(long indexNumber) {
        for (Student student : students) {
            if (student.getIndexNumber() == indexNumber)
                return student;
        }
        return null;
    }

    public void printStudentInfo(long indexNumber) {
        System.out.println(getStudent(indexNumber));
    }

    public List<Student> getAllStudents() {
        return students;
    }

    public void printAllStudents() {
        for(Student s : students) {
            System.out.println(s);
        }
    }

    public int calculateStudentsAge(long indexNumber) {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        return year - getStudent(indexNumber).getYearOfBirth();
    }

    public float calculateGradesAvg(long indexNumber) {
        Student student = getStudent(indexNumber);

        int[] r = {
                student.getGrades().get(Subject.ALGEBRA_LINIOWA),
                student.getGrades().get(Subject.ANALIZA_MATEMATYCZNA),
                student.getGrades().get(Subject.MATEMATYKA_DYSKRETNA),
                student.getGrades().get(Subject.PROGRAMOWANIE),
                student.getGrades().get(Subject.STATYSTYKA)
        };

        int sum = 0;
        for(int x : r) {
            sum += x;
        }

        return (float)sum / 5;
    }

    public void printLowRatingStudents() {
        for (Student student : students) {
            long index = student.getIndexNumber();
            float avg = calculateGradesAvg(index);
            if (avg < 3.5)
                System.out.println(student);
        }
    }

    public void sortStudentListByAge() {
        int n = students.size();

        Student a, b;

        do {
            for (int i = 0; i < n - 1; i++) {
                a = students.get(i);
                b = students.get(i+1);
                if (calculateStudentsAge(a.getIndexNumber()) > calculateStudentsAge(b.getIndexNumber())) {
                    students.remove(i+1);
                    students.add(i+1,a);
                    students.remove(i);
                    students.add(i,b);
                }
            }
            n-=1;
        } while (n > 1);
    }
}
