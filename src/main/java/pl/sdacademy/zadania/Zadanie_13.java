package pl.sdacademy.zadania;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Zadanie_13 {
    public static void main(String[] args) {
        List<Integer> fibos = new ArrayList<>();

        System.out.println("Ciag fibonaciego.");
        int n = getValidInt();

        for (int i = 1; i <= n; i++) {
            fibos.add(calculateFibo(i));
        }

        System.out.println(fibos);
    }

    public static int calculateFibo(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1)
            return 1;
        else {
            return calculateFibo(n-1) + calculateFibo(n-2);
        }
    }

    public static int getValidInt() {
        Scanner sc = new Scanner(System.in);

        int number = 0;

        while(true) {
            System.out.println("Podaj liczbe: ");
            try {
                number = sc.nextInt();
                break;
            } catch(InputMismatchException e) {
                sc.nextLine();
                System.out.println("Bledne dane!");
            }
        }

        return number;
    }
}
