package pl.sdacademy.zadania;

public class Chodakowska {
    public static void main(String[] args) {
        System.out.println(calculateWeight('M',100f,5));
    }

    public static float calculateWeight(char sex, float weight, int time) {
        float ratio;

        if (sex == 'M')
            ratio = 0.985f;
        else if (sex == 'K')
            ratio = 0.988f;
        else
            ratio = 1.0f;

        float currentWeight = weight;

        for (int i = 0; i < time; i++) {
            currentWeight *= ratio;
        }

        return currentWeight;
    }
}
