package pl.sdacademy.zadania;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadanie_11 {
    public static void main(String[] args) {

        System.out.println("Podaj x! (x >= 1):");
        int number;

        do {
            number = getValidInt();
        } while (number <= 1);
        int sum = 1;

        for (int i = 1; i <= number; i++) {
            sum *= i;
        }

        System.out.println(number + "! = " + sum);
    }

    public static int getValidInt() {
        Scanner sc = new Scanner(System.in);

        int number = 0;

        while(true) {
            System.out.println("Podaj liczbe: ");
            try {
                number = sc.nextInt();
                break;
            } catch(InputMismatchException e) {
                sc.nextLine();
                System.out.println("Bledne dane!");
            }
        }

        return number;
    }
}
