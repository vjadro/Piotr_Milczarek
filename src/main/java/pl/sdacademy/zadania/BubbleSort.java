package pl.sdacademy.zadania;

import java.util.ArrayList;
import java.util.List;

public class BubbleSort {
    private List<Integer> list;

    public BubbleSort(int... list) {
        this.list = new ArrayList<>();

        for (int i = 0; i < list.length; i++) {
            this.list.add(list[i]);
        }
    }

    public List<Integer> getSortedList() {

        int n = list.size();

        int a,b;

        do {
            for (int i = 0; i < n - 1; i++) {
                a = list.get(i);
                b = list.get(i+1);
                if (a > b) {
                    list.remove(i+1);
                    list.add(i+1,a);
                    list.remove(i);
                    list.add(i,b);
                }
            }
            n-=1;
        } while (n > 1);

        return list;
    }
}
