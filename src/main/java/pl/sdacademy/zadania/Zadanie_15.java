package pl.sdacademy.zadania;

public class Zadanie_15 {
    public static void main(String[] args) {

        if (isPrime(221423423))
            System.out.println("jest");
        else
            System.out.println("nie jest");
    }

    public static boolean isPrime(int n) {
        if (n < 2)
            return false;

        for (int i = 2; i*i < n; i++) {
            if (n % i == 0) return false;
        }

        return true;
    }
}
