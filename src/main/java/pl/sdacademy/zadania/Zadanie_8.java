package pl.sdacademy.zadania;

import java.util.Scanner;

public class Zadanie_8 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj jakieś słowo: ");
        String word = sc.nextLine();

        int wordLength = word.length();

        System.out.println(word.substring(0,Math.round((float)wordLength/2)));
        System.out.println("Dlugość słowa: " + wordLength);
    }
}
