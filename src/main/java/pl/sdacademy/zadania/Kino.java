package pl.sdacademy.zadania;

public class Kino {
    public static void main(String[] args) {
        System.out.println(daysForReturn(10f,0.1f,10f));
    }

    public static int daysForReturn(float passPrice, float discount, float ticketPrice) {
        double priceAfterDiscount = ticketPrice * discount;
        return (int)Math.ceil(passPrice / priceAfterDiscount);
    }
}
