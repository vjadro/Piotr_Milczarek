package pl.sdacademy.zadania;

public class Tablica2D {
    public static final int TABLE_WIDTH = 20;
    public static final int TABLE_HEIGHT = 15;

    static int[][] array;

    public static void main(String[] args) {

        array = new int[TABLE_WIDTH][TABLE_HEIGHT];

        for (int j = 0; j < TABLE_HEIGHT; j++) {
            for (int i = 0; i < TABLE_WIDTH; i++) {
                array[i][j] = 0;
            }
        }

        array[1][2] = 1;
        array[1][5] = 1;
        array[2][2] = 1;
        array[2][4] = 1;
        array[5][2] = 1;

        int sum = 0;

        for (int[] col : array) {
            for (int x : col) {
                if (x == 1) sum++;
            }
        }

        System.out.println(sum);
    }
}
