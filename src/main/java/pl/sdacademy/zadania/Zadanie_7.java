package pl.sdacademy.zadania;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Zadanie_7 {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();

        int number;

        System.out.println("Podawaj liczby calkowite, lub 0 aby zakończyć.");

        do {
            number = getValidInt();
            if (number != 0) numbers.add(number);
        } while(number != 0);

        StringBuilder allNumbers = new StringBuilder();
        StringBuilder allOdds = new StringBuilder();
        StringBuilder allEvens = new StringBuilder();

        int numberOfOdds = 0;
        int numberOfEvens = 0;

        for (int num : numbers) {
            allNumbers.append(num + " ");

            if (num % 2 == 0) {
                numberOfEvens++;
                allEvens.append(num + " ");
            } else {
                numberOfOdds++;
                allOdds.append(num + " ");
            }
        }

        System.out.println("Liczba wszystkich elementów: " + numbers.size());
        System.out.println("Liczba parzystych: " + numberOfEvens);
        System.out.println("Liczba nieparzystych: " + numberOfOdds);
        System.out.println("Wszystkie: " + allNumbers);
        System.out.println("Parzyste: " + allEvens);
        System.out.println("Nieparzyste: " + allOdds);
    }

    public static int getValidInt() {
        Scanner sc = new Scanner(System.in);

        int number = 0;

        while(true) {
            System.out.println("Podaj liczbe: ");
            try {
                number = sc.nextInt();
                break;
            } catch(InputMismatchException e) {
                sc.nextLine();
                System.out.println("Bledne dane!");
            }
        }

        return number;
    }
}
