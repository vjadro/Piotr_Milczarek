package pl.sdacademy.zadania.studenci;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Dziekanat dziekanat = new Dziekanat();

        Map<Subject, Integer> grades1 = new HashMap<>();
        Map<Subject, Integer> grades2 = new HashMap<>();
        Map<Subject, Integer> grades3 = new HashMap<>();
        Map<Subject, Integer> grades4 = new HashMap<>();

        grades1.put(Subject.ALGEBRA_LINIOWA,5);
        grades1.put(Subject.ANALIZA_MATEMATYCZNA,3);
        grades1.put(Subject.STATYSTYKA,3);
        grades1.put(Subject.PROGRAMOWANIE,2);
        grades1.put(Subject.MATEMATYKA_DYSKRETNA,2);
        dziekanat.addStudent(new Student("Piotr","Milczarek",19294L,1989,grades1));

        grades2.put(Subject.ALGEBRA_LINIOWA,2);
        grades2.put(Subject.ANALIZA_MATEMATYCZNA,4);
        grades2.put(Subject.STATYSTYKA,3);
        grades2.put(Subject.PROGRAMOWANIE,3);
        grades2.put(Subject.MATEMATYKA_DYSKRETNA,5);
        dziekanat.addStudent(new Student("Tomek","Ogórek",192394L,1999,grades2));

        grades3.put(Subject.ALGEBRA_LINIOWA,4);
        grades3.put(Subject.ANALIZA_MATEMATYCZNA,3);
        grades3.put(Subject.STATYSTYKA,3);
        grades3.put(Subject.PROGRAMOWANIE,5);
        grades3.put(Subject.MATEMATYKA_DYSKRETNA,4);
        dziekanat.addStudent(new Student("Jarek","Pindololo",19297L,1994,grades3));

        grades4.put(Subject.ALGEBRA_LINIOWA,5);
        grades4.put(Subject.ANALIZA_MATEMATYCZNA,3);
        grades4.put(Subject.STATYSTYKA,5);
        grades4.put(Subject.PROGRAMOWANIE,5);
        grades4.put(Subject.MATEMATYKA_DYSKRETNA,5);
        dziekanat.addStudent(new Student("Kujonus","Maximus",192932L,1991,grades4));

        dziekanat.sortStudentListByAge();
        dziekanat.printAllStudents();
        System.out.println();
        dziekanat.printLowRatingStudents();
    }
}
