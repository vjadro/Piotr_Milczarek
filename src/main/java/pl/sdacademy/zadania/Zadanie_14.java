package pl.sdacademy.zadania;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadanie_14 {
    public static void main(String[] args) {
        System.out.println("Znajdowanie NWD. Podaj 2 liczby a i b.");

        int a = getValidInt();
        int b = getValidInt();

        System.out.println("NWD(" + a + "," + b +") = " + findNWD(a,b));
    }

    public static int findNWD(int a, int b) {
        while (b != 0) {
            int c = a % b;
            a = b;
            b = c;
        }
        return a;
    }

    public static int getValidInt() {
        Scanner sc = new Scanner(System.in);

        int number = 0;

        while(true) {
            System.out.println("Podaj liczbe: ");
            try {
                number = sc.nextInt();
                break;
            } catch(InputMismatchException e) {
                sc.nextLine();
                System.out.println("Bledne dane!");
            }
        }

        return number;
    }
}
