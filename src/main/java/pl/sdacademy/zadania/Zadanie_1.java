package pl.sdacademy.zadania;

import java.util.Scanner;

public class Zadanie_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number;

        System.out.println("Podaj liczbę 0+:");
        try {
            number = sc.nextInt();
        } catch (NumberFormatException e) {
            System.out.println("Błędna wartość! Nara!");
            return;
        }

        if (number < 0) {
            System.out.println("Liczba ma być większa od 0. Nara!");
            return;
        }

        System.out.println(isEven(number) ? "Liczba jest parzysta" : "Liczba nie jest parzysta");
    }

    public static boolean isEven(int number) {
        return number%2 == 0 ? true : false;
    }
}
