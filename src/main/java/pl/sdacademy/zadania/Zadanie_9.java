package pl.sdacademy.zadania;

import java.util.Scanner;

public class Zadanie_9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Sprawdz czy slowo jest palindromem.");
        System.out.println("Podaj słowo: ");
        String word = sc.nextLine();

        word = word.toLowerCase();

        StringBuilder wordReversed = new StringBuilder();

        for (int i = word.length()-1; i >= 0; i--) {
            wordReversed.append(word.charAt(i));
        }

        if (word.equals(wordReversed.toString()))
            System.out.println("TAK");
        else
            System.out.println("NIE");
    }
}
