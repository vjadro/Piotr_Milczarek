package pl.sdacademy.zadania;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadanie_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] numbers = new int[5];

        System.out.println("Podaj 5 liczb:");
        try {
            numbers[0] = sc.nextInt();
            numbers[1] = sc.nextInt();
            numbers[2] = sc.nextInt();
            numbers[3] = sc.nextInt();
            numbers[4] = sc.nextInt();
        } catch(InputMismatchException e) {
            System.out.println("Dupa, nara!");
            return;
        }

        int sum = 0;

        for (int number : numbers) {
            if (isEven(number)) sum += number;
            else sum -= number;
        }

        System.out.println(sum);
    }

    public static boolean isEven(int number) {
        return number%2 == 0 ? true : false;
    }
}
