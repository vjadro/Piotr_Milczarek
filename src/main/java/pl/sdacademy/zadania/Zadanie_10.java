package pl.sdacademy.zadania;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class Zadanie_10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj jakieś zdanie lub ciąg słów:");
        String sentence = sc.nextLine();

        sentence = sentence.replaceAll("[.,]","");

        String[] words = sentence.split(" ");

        int shortestLength = Integer.MAX_VALUE;
        int longestLength = Integer.MIN_VALUE;
        String shortestWord = "";
        String longestWord = "";

        HashSet<String> ts = new HashSet<>();

        for (String word : words) {
            if (word.length() < shortestLength) {
                shortestLength = word.length();
                shortestWord = word;
            }
            if (word.length() > longestLength) {
                longestLength = word.length();
                longestWord = word;
            }
            ts.add(word);
        }

        System.out.println("Najdluższe słowo: " + longestWord);
        System.out.println("Najkrótsze słowo: " + shortestWord);

        int mostFrequentNumber = 1;
        int lessFrequentNumber = 1;

        Iterator<String> iterator = ts.iterator();
        while (iterator.hasNext()) {
            int occurrences = findNumberOfOccurrences(iterator.next(),words);
            if (occurrences > mostFrequentNumber)
                mostFrequentNumber = occurrences;
            if (occurrences < lessFrequentNumber)
                lessFrequentNumber = occurrences;
        }

        StringBuilder mostFrequentWords = new StringBuilder();
        StringBuilder lessFrequentWords = new StringBuilder();

        iterator = ts.iterator();
        while (iterator.hasNext()) {
            String w = iterator.next();
            int occurrences = findNumberOfOccurrences(w,words);

            if (occurrences == mostFrequentNumber) {
                mostFrequentWords.append(w).append(" ");
            }
            if (occurrences == lessFrequentNumber) {
                lessFrequentWords.append(w).append(" ");
            }
        }

        System.out.println("Najczęściej występujące wyrazy: " + mostFrequentWords);
        System.out.println("Najrzadziej występujące wyrazy: " + lessFrequentWords);
    }

    private static int findNumberOfOccurrences(String word, String[] listOfWords) {
        int result = 0;

        for (String w : listOfWords) {
            if (w.equals(word)) result++;
        }

        return result;
    }
}
