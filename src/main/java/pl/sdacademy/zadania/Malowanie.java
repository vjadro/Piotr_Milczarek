package pl.sdacademy.zadania;

public class Malowanie {
    static int[] frequencyTable = new int[10];

    public static void main(String[] args) {
        calculateFrequencyTable(125,132);
        for(int n : frequencyTable) {
            System.out.print(n + " ");
        }
    }

    public static void calculateFrequencyTable(int firstNumber, int lastNumber) {
        for (int i = firstNumber; i <= lastNumber; i++) {
            separateNumber(i);
        }
    }

    public static void separateNumber(int num) {
        if (num < 10) {
            frequencyTable[num]++;
            return;
        } else {
            frequencyTable[num%10]++;
            separateNumber(num/10);
        }
    }
}
