package pl.sdacademy.zadania;

public class Zadanie_4 {
    public static void main(String[] args) {
        int[] numbers = {10,11,12,13,14,15,16,17,18,19,20};
        int sum = 0;

        for (int number : numbers) {
            sum += number;
        }

        System.out.println((float)sum / numbers.length);
    }
}
