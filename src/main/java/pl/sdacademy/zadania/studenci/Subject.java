package pl.sdacademy.zadania.studenci;

public enum Subject {
    ANALIZA_MATEMATYCZNA, ALGEBRA_LINIOWA, STATYSTYKA, MATEMATYKA_DYSKRETNA, PROGRAMOWANIE
}
