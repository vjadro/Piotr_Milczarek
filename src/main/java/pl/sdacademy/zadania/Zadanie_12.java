package pl.sdacademy.zadania;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadanie_12 {
    public static void main(String[] args) {
        System.out.println("Podaj x! (x >= 1):");
        int number;

        do {
            number = getValidInt();
        } while (number <= 1);

        System.out.println(number + "! = " + getFactorial(number));
    }

    public static int getFactorial(int x) {
        if (x < 2) return 1;
        return x*getFactorial(x-1);
    }

    public static int getValidInt() {
        Scanner sc = new Scanner(System.in);

        int number = 0;

        while(true) {
            System.out.println("Podaj liczbe: ");
            try {
                number = sc.nextInt();
                break;
            } catch(InputMismatchException e) {
                sc.nextLine();
                System.out.println("Bledne dane!");
            }
        }

        return number;
    }
}
